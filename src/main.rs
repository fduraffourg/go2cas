use clap::Clap;
use reqwest;
use scraper::Html;
use scraper::Selector;
use std::collections::HashMap;
use std::fmt;
use std::fs::File;
use std::io::Write;
use std::process::Command;
use std::process::Stdio;
use std::str;
use std::time::{SystemTime, UNIX_EPOCH};

const USER_AGENT: &str = "Mozilla/5.0 (X11; Linux x86_64; rv:83.0) Gecko/20100101 Firefox/83.0";
const LOGIN_URL: &str = "https://go2top.cas-diablerets.ch/application/index.php?page=loginUser";
const CAPTCHA_URL: &str ="https://go2top.cas-diablerets.ch//application/protected/classes/utils/MathCaptcha/generateCaptcha.php";

#[derive(Clap)]
#[clap(version = "1.0", about = "Register to a CAS outing")]
struct Opts {
    #[clap(short, long)]
    user: String,
    #[clap(short, long)]
    pass: String,
    #[clap(short, long)]
    activity: u32,
}

struct MainError {
    msg: String,
}

impl fmt::Debug for MainError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&self.msg, f)
    }
}

fn main() -> Result<(), MainError> {
    let opts: Opts = Opts::parse();

    let activity_url = format!( "https://go2top.cas-diablerets.ch/application/index.php?page=ca.activiteDetail&activite_id={}&from=home", opts.activity.to_string());
    println!("activity_url: {:?}", activity_url);

    let client = reqwest::blocking::Client::builder()
        .user_agent(USER_AGENT)
        .cookie_store(true)
        .redirect(reqwest::redirect::Policy::none())
        .build()
        .map_err(|e| MainError {
            msg: format!("Failed to create reqwest client: {}", e),
        })?;

    println!("Login");
    login(&client, &opts.user, &opts.pass)?;
    println!("Login successful");

    println!("Get activity page");
    let (registered, mut inputs) = get_activity_page_infos(&client, &activity_url)?;
    if registered {
        println!("You are already registered, do nothing");
        std::process::exit(0);
    }

    let captcha_response = solve_captcha(&client)?;
    println!("captcha_response: {:?}", captcha_response);

    inputs.insert("ctl0$Main$myResponse".to_string(), captcha_response);
    inputs.insert(
        "PRADO_POSTBACK_TARGET".to_string(),
        "ctl0$Main$ctl6".to_string(),
    );
    inputs.insert("ctl0_Main_onglets_1".to_string(), "2".to_string());
    inputs.iter().for_each(|(k, v)| {
        if k != "PRADO_PAGESTATE" {
            println!(" - {:?} : {:?}", k, v);
        }
    });

    register(&client, &activity_url, inputs)?;

    let (registered, _) = get_activity_page_infos(&client, &activity_url)?;
    if registered {
        println!("OK!!!");
        Ok(())
    } else {
        println!("NOT OK!!!");
        Err(MainError {
            msg: "Registration failed, the user doesn't appear on the list of registered people"
                .to_string(),
        })
    }
}

fn login(client: &reqwest::blocking::Client, user: &str, password: &str) -> Result<(), MainError> {
    let mut login_data = HashMap::new();
    login_data.insert("ctl0$Main$login", user);
    login_data.insert("ctl0$Main$password", password);
    login_data.insert("PRADO_POSTBACK_TARGET", "ctl0$Main$ctl5");
    login_data.insert("PRADO_PAGESTATE", "eJzdVU2P0zAQ3Z9S5cIJKXY+mrgntmhBiJZqt3BFbux0LZy4xA7bapX/jp2vunEvsOLAntq8Gc/Mmxk/Z9APdzEOMj8PiJ+nIclSkuM0joIkIGGSkAhENE8xguhZohB5meK+t3C/AXpmyO/+SBQhb8sUp95CogB5y3cP3qJphgOgDRCcI0AD+Oh59AimQDgFoikQT4H5FEimQHoGoq4w30GAg0AHCRwkdJDIQWIHmTtI4iBOzdCqOUFeQcv6q6SVAaN+FMbL4QEdHtDiASDyDpXIGafyM5PK8R0ZGuegy7sUZUmPTJTjgvTOI3lrRebI+8Yk25kl2Wm46WJjTit1sVDDpm3pUZl98pFnfq6d114rzNr07XaZwrjYs3KDpXwSFWkDg8nqjrxa16vL/KBOJs8XFOs6uo+uu0Abbzrk5nvOKCfSamJgG0Wpy1/rykFowRnXpVm0oG8ba6lE0Sc3RyGwrITJA8en3tzWeceOlJirdnnXXsxnOJcJLqr+Uldtpj9iuf4HBOHF+h/sQb+eKQavfYrh/0NQn0m13uFKi8VbTnOFZmA+PxwX3l8xf8lD1tPKHoWQdNPK9bkPQCvkPSZMGP12rFrfs1H9ml7xfzH6tKJS4j2Vo4brW/UeK/yhYldulc6xFLwuysHf0mygW/iRYkKrQbpB3Mk4nZF6VnSJDJ52Ke7MIHttz/SE9JOiW8UQGPTcegW0y6c3mM143QbWqW5rpUS5PR3ogGxq+dihLQJMrUWBS7LGxeiEsx+roZLGmkt6bk5jv1swMmFKVQku7+nPmlWs3G+EVLc60vnMb7sh4Ok=");

    let resp = client
        .post(LOGIN_URL)
        .form(&login_data)
        .send()
        .map_err(|e| MainError {
            msg: format!("Failed to login: {}", e),
        })?;

    if resp.status() == reqwest::StatusCode::FOUND {
        Ok(())
    } else {
        Err(MainError {
            msg: format!("Login failed, get a HTTP status code {}", resp.status()),
        })
    }
}

/* Return a boolean to indicate if the user is registered already and a HashMap containing all the
 * input fields of the page (to register if needed)
 */
fn get_activity_page_infos(
    client: &reqwest::blocking::Client,
    activity_url: &str,
) -> Result<(bool, HashMap<String, String>), MainError> {
    let get_resp = client.get(activity_url).send().map_err(|e| MainError {
        msg: format!("Failed to get activity page: {}", e),
    })?;
    let body = get_resp
        .text()
        .map(|t| t.to_string())
        .map_err(|e| MainError {
            msg: format!("Failed to parse text from activity page response: {}", e),
        })?;

    let html = Html::parse_document(&body);

    // Check the registration status
    let input_selector =
        Selector::parse("#ctl0_Main_participantGrid td").map_err(|e| MainError {
            msg: format!("Failed to parse css selector: {:?}", e),
        })?;
    let me = html.select(&input_selector).find(|td| {
        let content = td.inner_html();
        content == "Florian Duraffourg"
    });
    let registered = me.is_some();

    // Extract input fields
    let input_selector = Selector::parse("input").map_err(|e| MainError {
        msg: format!("Failed to parse css selector: {:?}", e),
    })?;

    let inputs = html
        .select(&input_selector)
        .map(|input| {
            let name = input.value().attr("name").unwrap_or("").to_string();
            let value = if input.value().attr("type") == Some("checkbox") {
                "on".to_string()
            } else {
                input.value().attr("value").unwrap_or("").to_string()
            };
            (name, value)
        })
        .collect();

    Ok((registered, inputs))
}

fn solve_captcha(client: &reqwest::blocking::Client) -> Result<String, MainError> {
    let captcha = client.get(CAPTCHA_URL).send().map_err(|e| MainError {
        msg: format!("Failed to get captcha: {}", e),
    })?;
    assert!(captcha.status() == reqwest::StatusCode::OK);

    let mut file = File::create("/tmp/captcha.png").map_err(|e| MainError {
        msg: format!("Failed to create captch file: {}", e),
    })?;
    let all_bytes = &captcha.bytes().map_err(|e| MainError {
        msg: format!("Failed to get all bytes from the captcha response: {}", e),
    })?;
    file.write_all(all_bytes).map_err(|e| MainError {
        msg: format!("Failed to write captcha to file: {}", e),
    })?;

    println!("Captcha retrieved");

    Command::new("convert")
        .args(&[
            "/tmp/captcha.png",
            "-colorspace",
            "Gray",
            "-depth",
            "8",
            "-resample",
            "200",
            "-flatten",
            "-alpha",
            "Off",
            "/tmp/captcha-for-reading.png",
        ])
        .output()
        .map_err(|e| MainError {
            msg: format!("Failed to run 'convert' command: {}", e),
        })?;

    let tesseract_out = Command::new("tesseract")
        .args(&["/tmp/captcha-for-reading.png", "stdout", ">", "captcha.txt"])
        .output()
        .map_err(|e| MainError {
            msg: format!("Failed to run 'tesseract' command: {}", e),
        })?;
    let tesseract_txt = str::from_utf8(&tesseract_out.stdout).map_err(|e| MainError {
        msg: format!("Fail to get output from tesseract, {}", e),
    })?;
    let captcha_txt = tesseract_txt
        .trim()
        .trim_end_matches('7')
        .trim_end_matches("7?")
        .trim_end_matches('?');
    println!("captcha_txt: {:?}", captcha_txt);

    let mut bc = Command::new("bc")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .map_err(|e| MainError {
            msg: format!("Failed to run 'bc' command: {}", e),
        })?;
    {
        let mut stdin = bc.stdin.take().ok_or(MainError {
            msg: "Failed to get stdin".to_string(),
        })?;
        stdin
            .write_all(captcha_txt.as_bytes())
            .map_err(|e| MainError {
                msg: format!("Failed to write captcha text to stdin: {}", e),
            })?;
        stdin.write_all("\n".as_bytes()).map_err(|e| MainError {
            msg: format!("Failed to write newline to stdin: {}", e),
        })?;
        stdin.flush().map_err(|e| MainError {
            msg: format!("Failed to flush stdin: {}", e),
        })?;
    }

    let bc_output = bc.wait_with_output().map_err(|e| MainError {
        msg: format!("Failed to wait output from 'bc': {}", e),
    })?;
    if bc_output.status.success() {
        let result = str::from_utf8(&bc_output.stdout)
            .map(|o| o.trim())
            .map_err(|e| MainError {
                msg: format!("Failed to get output from 'bc': {}", e),
            })?;
        println!("bc result: {:?}", result);
        Ok(result.to_string())
    } else {
        Err(MainError {
            msg: "bc didn't returned properly".to_string(),
        })
    }
}

fn register(
    client: &reqwest::blocking::Client,
    activity_url: &str,
    inputs: HashMap<String, String>,
) -> Result<(), MainError> {
    let resp = client
        .post(activity_url)
        .form(&inputs)
        .send()
        .map_err(|e| MainError {
            msg: format!("Failed to post registration call: {}", e),
        })?;
    if resp.status() == reqwest::StatusCode::OK {
        // Save the content of the response to a file for debugging
        let epoch_millis = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .map(|d| d.as_millis())
            .map_err(|e| MainError {
                msg: format!("Failed to get the system time: {}", e),
            })?;
        let file_name = format!("/tmp/go2cas-register-{}", epoch_millis);
        println!("Write registration response to {}", &file_name);
        let mut file = File::create(&file_name).map_err(|e| MainError {
            msg: format!("Failed to create registration response file: {}", e),
        })?;
        let all_bytes = &resp.bytes().map_err(|e| MainError {
            msg: format!(
                "Failed to get all bytes from the registration response: {}",
                e
            ),
        })?;
        file.write_all(all_bytes).map_err(|e| MainError {
            msg: format!("Failed to save registration response to file: {}", e),
        })?;
        Ok(())
    } else {
        Err(MainError {
            msg: format!(
                "Registration POST request returned wrong status: {}",
                resp.status()
            ),
        })
    }
}
